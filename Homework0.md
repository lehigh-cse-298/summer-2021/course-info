# CSE 298 - Homework 0

**Due Date:** 7/9/2021 EOD

## Assignment

This is a short assignment that counts toward your Homework 1 grade. The purpose is to get you signed up for a Gitlab account.

1. Sign up for Gitlab: https://gitlab.com/users/sign_up. I would suggest to sign up for an account directly -- don't use any of the authorization providers. Your lehigh e-mail ID is a good choice for a username, but it doesn't matter what you use. If you already have an account, you don't need to make a new one.
2. Choose an avatar image for your Gitlab account. This doesn't havet to be a picture of yourself, but that's not a bad choice. Whatever it is, make sure it's appropriate for a classroom setting. You will use the same avatar on Slack and Gitlab.
3. Via Slack, send me a DM with a link to your Gitlab profile.
4. On Slack, set your avatar to be the same one you chose for your Gitlab profile. This will help me relate your two accounts together and to remember you.
5. Create a gew Gitlab Group - [Instructions](https://docs.gitlab.com/ee/user/group/)
  - The group name is particular. It needs to be of the form: `<your-lehigh-email>-cse298`. For example, my Lehigh e-mail is `cim310`, so my group would be named `cim310-cse298`. If your username is `abc123`, your group would be named `abc123-cse298`. Note, the name you use here is not your Gitlab username, but your *Lehigh e-mail id*. It's important to get this right, because some of the course tools will assume this to be the case. **If your name deviates from what I've indicated above, the course tools will not be able to find your assignments.**.
  - Make sure your group's visibility is set to **Private**. This is to make sure no other student can see your work.
  - Under the Membership tab of your project's dashboard, invite the member `@LehighCSE298` to your group as a `Maintainer` - [Instructions](https://docs.gitlab.com/ee/user/project/members/#members-of-a-project)
