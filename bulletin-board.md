# CSE 298 - Summer 2021 - Bulletin Board

## :computer: Homework

- [ ] 08/12 - [Homework 4](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/homework-2) - EKF Implementation
- [x] 07/26 - [Homework 3](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/homework-2) - EKF Motion Model Derivation
- [x] 07/19 - [Homework 2](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/homework-2) - Matlab Exercises
- [x] 07/09 - [Homework 1](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/homework-1) - Learning Git
- [x] 07/09 - [Homework 0](https://gitlab.com/lehigh-cse-298/summer-2021/course-info/-/blob/master/Homework0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

- [ ] 08/16 - [Exam Option 1](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/exam-1)
- [x] 07/28 - [Quiz 3](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/quiz-3)
- [x] 07/21 - [Quiz 2](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/quiz-2)
- [x] 07/14 - [Quiz 1](https://gitlab.com/lehigh-cse-298/summer-2021/assignments/quiz-1)

## :books: Readings

| Week                      | Readings           | 
| ------------------------- | ------------------ | 
| Week 6 | <ul><li>[Reinforcement Learning](http://web.stanford.edu/class/psych209/Readings/SuttonBartoIPRLBook2ndEd.pdf) - Chapters 1 and 6</li><li>[Trajectory Rollout](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.330.2120&rep=rep1&type=pdf)</li></ul>
| Week 3 | <ul><li>[Introduction to Autonomous Robots](https://github.com/correll/Introduction-to-Autonomous-Robots/releases/download/v1.9.2/book.pdf) - Chapters 11</li><li>[FastSLAM](http://robots.stanford.edu/papers/Thrun03g.pdf)</li></ul>
| Week 2 | [Introduction to Autonomous Robots](https://github.com/correll/Introduction-to-Autonomous-Robots/releases/download/v1.9.2/book.pdf) - Chapters 5, 7, 9 |
| Week 1 | [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6 |

## :vhs: Lectures - [Playlist](https://youtube.com/playlist?list=PL4A2v89SXU3SUUNrwKcE-yy2SX6YQOg_p)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
|**Week 6**|
|Lecture 10 | 8/10 | Reinforcement Learning | [Video](https://youtu.be/Cc1IP37R090)
|**Week 5**|
|Recitation 4 | 8/5 | Recitation 5 | [Video](https://drive.google.com/file/d/1zH9VrQBV76f0PIwP-sHiZxvpQCScBMr3/view?usp=sharing)
|Lecture 9 | 8/3 | A* | [Video](https://youtu.be/WSr2glTIttQ)
|**Week 4**|
|Recitation 4 | 7/29 | Recitation 4 | [Video](https://drive.google.com/file/d/1aN2TK60srb4Jhsozf9_F7rwkWhIUNC61/view?usp=sharing)
|Lecture 8 | 7/27 | Trajectory Rollout | [Video](https://youtu.be/buEfiJftc0E)
|Lecture 7 | 7/26 | Feedback Control | [Video](https://youtu.be/AZC8E7uUuac)
|**Week 3**|
|Recitation 3 | 7/22 | Recitation 3 | [Video](https://drive.google.com/file/d/1zH9VrQBV76f0PIwP-sHiZxvpQCScBMr3/view?usp=sharing)
|Lecture 6 | 7/20 | FastSLAM | [Video](https://youtu.be/tDftJ65MuWo)
|Lecture 5 | 7/19 | Particle Filter | [Video](https://youtu.be/IVbylUHI0oU)
|**Week 2**|
|Recitation 2 | 7/15 | Recitation 2 | [Video](https://drive.google.com/file/d/1WtCkL2t-YzWMHEU4UbES7A0cwfNRrJ54/view?usp=sharing)
|Lecture 4 | 7/13 | Extended Kalman Filter | [Video](https://youtu.be/i5kBVv4DB38)
|Lecture 3 | 7/12 | RANSAC and Intro to SLAM | [Video](https://youtu.be/AxSHyQ_APxc)
|**Week 1**|
|Recitation 1 | 7/8 | Recitation 1 | [Video](https://drive.google.com/drive/folders/1Z8K2o0iNE70arJC8uYm2LWnEYu2NqqNG?usp=sharing)
|Lecture 2 | 7/7 | Sensors | [Video](https://youtu.be/dLsOutDHlF4)
|Lecture 1 | 7/6 | Perception | [Video](https://youtu.be/09RD2dNwQew)
